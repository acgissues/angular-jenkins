pipeline {
    agent any

    environment {
        CHROME_BIN = '/usr/bin/google-chrome'
        NODE_VERSION = '8.12.0'
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '2'))
    }

    stages {
        stage('Install Dependencies') {
            steps {
                echo "${BRANCH_NAME}"
                nvm(version: "${env.NODE_VERSION}") {
                    sh 'npm install --verbose'
                }
            }
        }

        stage('Static Analysis') {
            steps {
                nvm(version: "${env.NODE_VERSION}") {
                    // Output format to be fixed.
                    sh 'npm run lint'
                }
            }
        }

        stage('Build') {
            steps {
                nvm(version: "${env.NODE_VERSION}") {
                    sh 'npm run build -prod'
                }
            }
        }

        stage('Unit Testing') {
            steps {
                nvm(version: "${env.NODE_VERSION}") {
                    sh 'npm run test'
                }
            }
            post {
                always {
                    junit allowEmptyResults: true, testResults: '**/test-results.xml'
                }
            }
        }

        stage('Deploy to production') {
            when {
                branch 'master'
            }
            steps {
                echo 'Deployment Steps'
            }
        }
    }
    post {
        always {
            echo 'Finished job.'
        }
        success {
            echo 'Build successful!'
        }
        failure {
            echo 'Build failed!'
        }
        unstable {
            echo 'Build unstable!'
        }
    }
}
